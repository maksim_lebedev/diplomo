﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;


namespace subnetscanner
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        public Form1()
        {
            InitializeComponent();

            lblStatus.ForeColor = System.Drawing.Color.Green;
            lblStatus.Text = "Ready for work";
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        Thread myThread = null;

        public void ScanIP(string subnet)
        {
            Ping myPing;
            PingReply reply;
            IPAddress addr;
            IPHostEntry host;

            progressBar1.Maximum = 254;
            progressBar1.Value = 0;
            listVAddr.Items.Clear();

            for (int i = 1; i < 255; i++)
            {
                string subnetn = "." + i.ToString();
                myPing = new Ping();
                reply = myPing.Send(subnet + subnetn, 900);

                lblStatus.ForeColor = System.Drawing.Color.Green;
                lblStatus.Text = "Scanning: " + subnet + subnetn;

                if (reply.Status == IPStatus.Success)
                {
                    try
                    {
                        addr = IPAddress.Parse(subnet + subnetn);
                        host = Dns.GetHostEntry(addr);

                        listVAddr.Items.Add(new ListViewItem(new String[] { subnet + subnetn, host.HostName }));
                    }
                    catch { listVAddr.Items.Add(new ListViewItem(new String[] { subnet + subnetn, "Unknown"})); }
                }
                progressBar1.Value += 1;
            }
            cmdScan.Enabled = true;
            cmdStop.Enabled = false;
            cmdScanPort.Enabled = true;
            txtIP.Enabled = true;
            lblStatus.Text = "Done";
        }

        public void ScanPort(string subnet, int startPort, int endPort)
        {
            progressBar1.Maximum = endPort - startPort + 1;
            progressBar1.Value = 0;
            listPort.Items.Clear();
            List<int> Ports = new List<int>();

            for (int currPort = startPort; currPort <= endPort; currPort++)
            {
                TcpClient TCPportScan = new TcpClient();
                lblStatus.ForeColor = System.Drawing.Color.Green;
                lblStatus.Text = "Scanning: " + currPort;
                try
                {
                    TCPportScan.Connect(subnet, currPort);
                    Ports.Add(currPort);
                    listPort.Items.Add(new ListViewItem(new string[] { "Port" + currPort, "Open" }));
                }
                catch {}
                progressBar1.Value += 1;
            }
            cmdScan.Enabled = true;
            cmdStop.Enabled = false;
            cmdScanPort.Enabled = true;
            txtIP.Enabled = true;
            lblStatus.Text = "Done";
        }

        public async void SearchVurln()
        {
            string ConStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\chern\source\repos\subnetscanner\subnetscanner\Vulnerability.mdf;Integrated Security=True";
            sqlConnection = new SqlConnection(ConStr);
            await sqlConnection.OpenAsync();

        }

        private void cmdScan_Click(object sender, EventArgs e)
        {
            if (txtIP.Text == string.Empty)
            {
                MessageBox.Show("No IP adress entered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                myThread = new Thread(() => ScanIP(txtIP.Text));
                myThread.Start();

                if (myThread.IsAlive == true)
                {
                    cmdScan.Enabled = false;
                    cmdStop.Enabled = true;
                    cmdScanPort.Enabled = false;
                    txtIP.Enabled = false;
                }
            }
        } 

        private void progressBar1_Click(object sender, EventArgs e)
        {
        }

        private void cmdScanPort_Click(object sender, EventArgs e)
        {
            int startPort = Convert.ToInt32(strPrt.Text);
            int endPort = Convert.ToInt32(endPrt.Text);

            if (txtPort.Text == string.Empty)
            {
                MessageBox.Show("No IP adress entered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else 
            {
                myThread = new Thread(() => ScanPort(txtPort.Text, startPort, endPort));
                myThread.Start();

                if (myThread.IsAlive == true)
                {
                    cmdScan.Enabled = true;
                    cmdStop.Enabled = false;
                    cmdScanPort.Enabled = false;
                    txtIP.Enabled = false;
                }
            }
        }
        private void cmdStop_Click(object sender, EventArgs e)
        {
            myThread.Suspend();
            cmdScan.Enabled = true;
            cmdStop.Enabled = false;
            cmdScanPort.Enabled = true;
            txtIP.Enabled = true;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
